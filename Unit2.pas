unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPComm, StdCtrls;

type
  TForm2 = class(TForm)
    Comm1: TComm;
    Memo1: TMemo;
    Button1: TButton;
    procedure Comm1ReceiveData(Sender: TObject; Buffer: Pointer;
      BufferLength: Word);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Comm1ReceiveData(Sender: TObject; Buffer: Pointer;
  BufferLength: Word);
var
  str:string;
begin
//
  str:=PChar(Buffer);
  Memo1.Lines.Add(str);
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
  Comm1.StartComm;
end;

end.
 