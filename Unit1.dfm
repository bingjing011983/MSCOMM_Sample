object Form1: TForm1
  Left = 110
  Top = 90
  Width = 1004
  Height = 544
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 81
    Width = 988
    Height = 425
    ActivePage = TabSheet2
    Align = alClient
    TabIndex = 1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #22320#30917#20202#34920
      object Label4: TLabel
        Left = 772
        Top = 44
        Width = 35
        Height = 13
        Caption = 'sleep'
      end
      object ListBox3: TListBox
        Left = 608
        Top = 40
        Width = 145
        Height = 301
        ImeName = #20013#25991'('#31616#20307') - '#30334#24230#36755#20837#27861
        ItemHeight = 13
        TabOrder = 0
      end
      object ListBox2: TListBox
        Left = 292
        Top = 40
        Width = 309
        Height = 301
        ImeName = #20013#25991'('#31616#20307') - '#30334#24230#36755#20837#27861
        ItemHeight = 13
        TabOrder = 1
      end
      object ListBox1: TListBox
        Left = 0
        Top = 40
        Width = 285
        Height = 301
        ImeName = #20013#25991'('#31616#20307') - '#30334#24230#36755#20837#27861
        ItemHeight = 13
        TabOrder = 2
      end
      object Edit2: TEdit
        Left = 532
        Top = 360
        Width = 141
        Height = 21
        TabOrder = 3
        Text = 'Edit2'
        OnDblClick = Edit2DblClick
      end
      object Edit1: TEdit
        Left = 368
        Top = 360
        Width = 149
        Height = 21
        TabOrder = 4
        Text = 'Edit1'
      end
    end
    object TabSheet2: TTabSheet
      Caption = #31216#37325#21464#36865#22120
      ImageIndex = 1
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 65
        Height = 13
        Caption = #35774#22791#22320#22336#65306
      end
      object Label7: TLabel
        Left = 332
        Top = 16
        Width = 78
        Height = 13
        Caption = #23492#23384#22120#25968#37327#65306
      end
      object Label9: TLabel
        Left = 20
        Top = 52
        Width = 39
        Height = 13
        Caption = #21629#20196#65306
      end
      object Label10: TLabel
        Left = 16
        Top = 120
        Width = 39
        Height = 13
        Caption = #25509#25910#65306
      end
      object Label8: TLabel
        Left = 168
        Top = 16
        Width = 78
        Height = 13
        Caption = #23492#23384#22120#22320#22336#65306
      end
      object cbo_DevAddr: TComboBox
        Left = 84
        Top = 12
        Width = 69
        Height = 21
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 0
        Text = '0'
        Items.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9'
          'A'
          'B'
          'C'
          'D'
          'E'
          'F')
      end
      object cbo_RegNum: TComboBox
        Left = 412
        Top = 12
        Width = 49
        Height = 21
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 1
        Text = '1'
        Items.Strings = (
          '1'
          '2'
          '3'
          '4')
      end
      object ed_SendCommand: TEdit
        Left = 68
        Top = 48
        Width = 245
        Height = 21
        Color = clSkyBlue
        TabOrder = 2
        OnDblClick = ed_SendCommandDblClick
      end
      object btn_Send: TButton
        Left = 328
        Top = 44
        Width = 57
        Height = 25
        Caption = #21457#36865
        TabOrder = 3
        OnClick = btn_SendClick
      end
      object ed_SendStr: TEdit
        Left = 68
        Top = 76
        Width = 245
        Height = 21
        TabOrder = 4
      end
      object ListBox4: TListBox
        Left = 68
        Top = 116
        Width = 409
        Height = 197
        ImeName = #20013#25991'('#31616#20307') - '#30334#24230#36755#20837#27861
        ItemHeight = 13
        TabOrder = 5
      end
      object Memo1: TMemo
        Left = 484
        Top = 8
        Width = 485
        Height = 373
        Lines.Strings = (
          '')
        ScrollBars = ssVertical
        TabOrder = 6
      end
      object cbo_RegAddr: TComboBox
        Left = 248
        Top = 12
        Width = 65
        Height = 21
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 7
        Text = '0'
        Items.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9'
          'A'
          'B'
          'C'
          'D'
          'E'
          'F')
      end
    end
  end
  object pnl_COMSetting: TPanel
    Left = 0
    Top = 0
    Width = 988
    Height = 81
    Align = alTop
    BevelOuter = bvLowered
    Color = clGradientActiveCaption
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 46
      Height = 13
      Caption = #31471#21475#21495':'
    end
    object Label2: TLabel
      Left = 156
      Top = 16
      Width = 46
      Height = 13
      Caption = #27874#29305#29575':'
    end
    object Label3: TLabel
      Left = 296
      Top = 16
      Width = 46
      Height = 13
      Caption = #32531#20914#21306':'
    end
    object Label5: TLabel
      Left = 436
      Top = 16
      Width = 59
      Height = 13
      Caption = #35302#21457#20107#20214':'
    end
    object Label11: TLabel
      Left = 16
      Top = 52
      Width = 39
      Height = 13
      Caption = #36830#25509#65306
    end
    object portNo: TEdit
      Left = 64
      Top = 12
      Width = 65
      Height = 21
      ImeName = #20013#25991'('#31616#20307') - '#30334#24230#36755#20837#27861
      TabOrder = 0
      Text = '1'
    end
    object bitRate: TEdit
      Left = 212
      Top = 12
      Width = 65
      Height = 21
      ImeName = #20013#25991'('#31616#20307') - '#30334#24230#36755#20837#27861
      TabOrder = 1
      Text = '9600'
    end
    object inputbuffersize: TEdit
      Left = 352
      Top = 12
      Width = 65
      Height = 21
      ImeName = #20013#25991'('#31616#20307') - '#30334#24230#36755#20837#27861
      TabOrder = 2
      Text = '256'
    end
    object ed_RThreshold: TEdit
      Left = 500
      Top = 12
      Width = 61
      Height = 21
      ImeName = #20013#25991'('#31616#20307') - '#30334#24230#36755#20837#27861
      TabOrder = 3
      Text = '32'
    end
    object CheckBox1: TCheckBox
      Left = 588
      Top = 16
      Width = 97
      Height = 17
      Caption = #24322#25110#26657#39564
      TabOrder = 4
    end
    object Comm1: TMSComm
      Left = 836
      Top = 5
      Width = 32
      Height = 32
      OnComm = Comm1Comm
      ControlData = {
        2143341208000000ED030000ED03000001568A64000006000A00010000040000
        00020000802500000000080000000000000000003F00000005000000}
    end
    object btn_Stop: TButton
      Left = 336
      Top = 48
      Width = 69
      Height = 25
      Caption = #20572#27490
      TabOrder = 6
      OnClick = btn_StopClick
    end
    object btn_Start: TButton
      Left = 260
      Top = 48
      Width = 65
      Height = 25
      Caption = #24320#22987
      TabOrder = 7
      OnClick = btn_StartClick
    end
    object btn_Clear: TButton
      Left = 704
      Top = 8
      Width = 75
      Height = 25
      Caption = #28165#23631
      TabOrder = 8
      OnClick = btn_ClearClick
    end
    object ed_ConnStr: TEdit
      Left = 64
      Top = 48
      Width = 149
      Height = 21
      TabOrder = 9
      Text = 'n,8,1'
    end
  end
end
