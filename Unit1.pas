unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, OleCtrls, MSCommLib_TLB, MPlayer, ComCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    btn_Start: TButton;
    portNo: TEdit;
    bitRate: TEdit;
    btn_Stop: TButton;
    CheckBox1: TCheckBox;
    inputbuffersize: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    ed_RThreshold: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Comm1: TMSComm;
    Label5: TLabel;
    btn_Clear: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    ListBox2: TListBox;
    ListBox3: TListBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    pnl_COMSetting: TPanel;
    Label6: TLabel;
    cbo_DevAddr: TComboBox;
    Label7: TLabel;
    cbo_RegNum: TComboBox;
    ed_SendCommand: TEdit;
    Label9: TLabel;
    btn_Send: TButton;
    Label10: TLabel;
    ed_SendStr: TEdit;
    ListBox4: TListBox;
    Memo1: TMemo;
    ed_ConnStr: TEdit;
    Label11: TLabel;
    Label8: TLabel;
    cbo_RegAddr: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure btn_StartClick(Sender: TObject);
    procedure Comm1Comm(Sender: TObject);
    procedure btn_StopClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btn_ClearClick(Sender: TObject);
    procedure Edit2DblClick(Sender: TObject);
    procedure ed_SendCommandDblClick(Sender: TObject);
    procedure btn_SendClick(Sender: TObject);
  private
    { Private declarations }
    ModBus_SendBuff:array[0..7] of Byte;
    str_ToSend:array[0..10] of Char;
    function ParityCheck(arrChar: PChar): Integer;
    function GetDeviceNo(arrChar: PChar): String;
    procedure ReceiveDiBang();
    procedure ReceiveFangDaQi();
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
uses StrUtils,ModbusUtils,ModbusConsts;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var
  i:Integer;
begin
//
end;

procedure TForm1.btn_StartClick(Sender: TObject);
var
  timestr:string;
begin
  with Comm1 do
  begin
    if PortOpen then PortOpen:=False;             //关闭端口
    CommPort:=StrToInt(portNo.Text);              //COM端口号
    InBufferSize:=StrToInt(inputbuffersize.Text);     //接收缓冲区-字节
    OutBufferSize:=10;          //发送缓冲区-字节
    Settings:=bitRate.Text+','+ed_ConnStr.Text;   //波特率，无校验，8位数据，1位停止
    InputLen:=0;                                  //读取缓冲区全部内容(32个字节)
    InBufferCount:=0;                             //清除接收缓冲区
    OutBufferCount:=0;                            //清除发送缓冲区
    RThreshold:=StrToInt(ed_RThreshold.Text);     //接收？个字节产生OnComm
    InputMode:=comInputModeBinary;                //
    PortOpen:=True;                               //打开端口
  end;
  timestr:=FormatDateTime('HH:mm:ss zzz',Now());
  ListBox1.Items.Add(timestr + ' ' + '开始');
  ListBox4.Items.Add(timestr + ' ' + '开始');
end;

function TForm1.ParityCheck(arrChar: PChar): Integer;
var
  i:Integer;
  c:Byte;
  paritRslt: Char;
  valueStr:string;
  tempc:Byte;
begin
  Result:=0;   valueStr:='';
  if Length(arrChar)=0 then Exit;
  c:=0;
  for i:=1 to 8 do
  begin
    c:=(Ord(c) xor Ord(arrChar[i]));
    if i<8 then valueStr:=valueStr+arrChar[i];
  end;

  tempc:=c and $0F;
  if tempc<=9 then
    paritRslt:=Char(tempc+$0030)
  else
    paritRslt:=Char(tempc+$0037);
  if arrChar[10]<>paritRslt then Exit;
  tempc:=c shr 4;
  if tempc<=9 then
    paritRslt:=Char(tempc+$0030)
  else
    paritRslt:=Char(tempc+$0037);
  if arrChar[9]<>paritRslt then Exit;
  try
    Result:=StrToInt(valueStr);
  except
  end;
end;

procedure TForm1.Comm1Comm(Sender: TObject);
begin
  ReceiveFangDaQi();
end;

procedure TForm1.btn_StopClick(Sender: TObject);
var
  timestr:string;
begin
  Comm1.PortOpen:=False;
  timestr:=FormatDateTime('HH:mm:ss zzz',Now());
  ListBox1.Items.Add(timestr + ' ' + '关闭');
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if Comm1.PortOpen then Comm1.PortOpen:=False;
end;

procedure TForm1.btn_ClearClick(Sender: TObject);
begin
  ListBox1.Items.Clear;
  ListBox2.Items.Clear;
  ListBox3.Items.Clear;
  ListBox4.Items.Clear;
  Memo1.Lines.Clear;
end;

function TForm1.GetDeviceNo(arrChar: PChar): String;
var
  i:Integer;
  valueStr:string;
begin
  valueStr:='';
  if Length(arrChar)=0 then Exit;
  for i:=0 to Length(arrChar)-1 do
  begin
    valueStr:=valueStr+arrChar[i];
    //if i<8 then valueStr:=valueStr+arrChar[i];
  end;

  try
    Result:=valueStr;
  except
  end;
end;

procedure TForm1.Edit2DblClick(Sender: TObject);
var
  i:Integer;
  c:Byte;
  paritRslt: Char;
  arrChar:PChar;
  resultCharArr:string[2];
  valueStr:string;
  tempc:Byte;
begin
  arrChar:=PChar(Edit1.Text);
  valueStr:='';
  Edit2.Text:='';
  if Length(arrChar)=0 then Exit;
  c:=0;
  for i:=1 to 8 do
  begin
    c:=(Ord(c) xor Ord(arrChar[i]));
    if i<8 then valueStr:=valueStr+arrChar[i];
  end;

  tempc:=c and $0F;
  if tempc<=9 then
    paritRslt:=Char(tempc+$0030)
  else
    paritRslt:=Char(tempc+$0037);
  resultCharArr[1]:=paritRslt;
  //if arrChar[10]<>paritRslt then Exit;
  tempc:=c shr 4;
  if tempc<=9 then
    paritRslt:=Char(tempc+$0030)
  else
    paritRslt:=Char(tempc+$0037);
  resultCharArr[0]:=paritRslt;
  Edit2.Text:=resultCharArr;
end;

procedure TForm1.ed_SendCommandDblClick(Sender: TObject);
var
  sendData:array[0..5] of Byte;
  devAddr, regAddr, regNum:Word;
  CRCResult:Word;
  i:Integer;
  str:string;
begin
//
  devAddr:=StrToInt(cbo_DevAddr.Text);
  regAddr:=StrToInt(cbo_RegAddr.Text);
  regNum:=StrToInt(cbo_RegNum.Text);
  sendData[0]:=Byte(devAddr);
  sendData[1]:=Byte(mbfReadHoldingRegs);
  sendData[2]:=Hi(regAddr);
  sendData[3]:=Lo(regAddr);
  sendData[4]:=Hi(regNum);
  sendData[5]:=Lo(regNum);
  for i:=Low(sendData) to High(SendData) do ModBus_SendBuff[i]:=sendData[i];
  CRCResult:=CalculateCRC16(sendData);
  ModBus_SendBuff[6]:=Lo(CRCResult);
  ModBus_SendBuff[7]:=Hi(CRCResult);
  ed_SendCommand.Text:=BufferToHex(sendData);
  ed_SendStr.Text:=BufferToHex(ModBus_SendBuff);
end;

procedure TForm1.btn_SendClick(Sender: TObject);
var
  ole:OleVariant;
  i:Integer;
begin
  ole:=VarArrayCreate([0,7],varByte);
  for i:=0 to 7 do ole[i]:=ModBus_SendBuff[i];
  Comm1.OutBufferCount:=0;
  Comm1.Output:=ole;
  //Sleep(10);
  Application.ProcessMessages;
end;

procedure TForm1.ReceiveFangDaQi;
var
  buffer:OleVariant;
  timestr:string;
  receiveBuff:array of Byte;
  H,L,i:Integer;
begin
//
  case Comm1.CommEvent of
  comEvReceive: ;
  comEvSend:
    begin
      timestr:=FormatDateTime('HH:mm:ss zzz',Now());
      ListBox4.Items.Add(timestr + ' 发送' + ed_SendStr.Text);
      //Sleep(5);
      begin
        timestr:=FormatDateTime('HH:mm:ss zzz',Now());
        buffer:=Comm1.Input;   //读出数据后自动清除缓冲区str[1]~str[32]
        H:=VarArrayHighBound(buffer,1);
        L:=VarArrayLowBound(buffer,1);
        SetLength(receiveBuff,H-L+1);
        for i:=L to H do
        begin
          receiveBuff[i]:=buffer[i];
        end;
        Memo1.Lines.Add(timestr + ' 接收' + BufferToHex(receiveBuff));
      end;
    end;

  end;
end;

procedure TForm1.ReceiveDiBang;
var
  buffer:OleVariant;
  cStart:Char;
  str_com:string;
  str_dest:array[1..12] of Char;
  intArray:string;
  str,valstr:string;
  i,j,val:Integer;
  timestr:string;
begin
//
  case Comm1.CommEvent of
  comEvReceive:
    begin
      Sleep(10);
      buffer:=Comm1.Input;   //读出数据后自动清除缓冲区str[1]~str[32]
      str_com:=buffer;
      intArray:='';
      for i:=1 to Length(str_com) do
      begin
        intArray:=intArray+' '+IntToStr(ord(str_com[i]));
      end;
      for i:=1 to Length(str_com) do
      begin
        if Ord(str_com[i])<>$02 then Continue
        else Break;
      end;
      for j:=1 to 12 do
        str_dest[j]:=(str_com)[i+j-1];

      timestr:=FormatDateTime('HH:mm:ss zzz',Now());
      if CheckBox1.Checked then
      begin
        val:=ParityCheck(@str_dest);
        ListBox1.Items.Add(timestr + ' ' + IntToStr(val));
      end
      else
      begin
        //valStr:=GetDeviceNo(@str_dest);
        //valstr:=str;
        ListBox1.Items.Add(timestr + ' ' + str_com);
        ListBox3.Items.Add(str_dest);
        val:=ParityCheck(@str_dest);
        ListBox2.Items.Add(IntToStr(val));
        //ListBox2.Items.Add(IntToStr(ParityCheck(@str_dest)));
      end;
    end;
  end;

end;

{
procedure p_GetEnrollData();
var
  dwEnrollData:OleVariant;
  High,Low,i:Integer;
  sEnrollList:TStringList;
begin
  sEnrollList:=TStringList.Create;
  try

    for i:=Low to High do
    begin
      sEnrollList.Add(inttostr(dwEnrollData[i]))
    end;
    //把sEnrollList.Text 保存到数据库中
  finally
    sEnrollList.Free;
  end;
end;      }

end.
